import React, { Component } from 'react';
import './css/bootstrap.min.css';
import './css/bootstrap.theme.min.css';
import './App.css';

function clearState(clearing) {
  clearing.setState({
    name: '',
    fam: '',
    date:'',
    mail:'',
    phone:'',
    pass:'',
    confirm:false,
    disabledButton:true,

    nameError: false,
    famError: false,
    dateError: false,
    mailError: false,
    phoneError: false,
    passError: false,
    confirmError: false,
  })
}
class UsersList extends React.Component{
  render(){
    var users = this.props.users;
    var userlist = users.map((user, userID) => {
      return (
        <tr key={userID}>
          <td>{user.name}</td>
          <td>{user.fam}</td>
          <td>{user.mail}</td>
        </tr>
      );
    });

    return (
      <tbody>{userlist}</tbody>
    );
  }
}
class NameForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      fam: '',
      date:'',
      mail:'',
      phone:'',
      pass:'',
      confirm:false,
      disabledButton:true,

      nameError: false,
      famError: false,
      dateError: false,
      mailError: false,
      phoneError: false,
      passError: false,
      confirmError: false,

      users: [],
    }

    this.handleName = this.handleName.bind(this)
    this.handleFam = this.handleFam.bind(this)
    this.handleDate = this.handleDate.bind(this)
    this.handleMail = this.handleMail.bind(this)
    this.handlePhone = this.handlePhone.bind(this)
    this.handlePass = this.handlePass.bind(this)
    this.handleConfirm = this.handleConfirm.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)

    this.checkForm = this.checkForm.bind(this)
  }

  checkForm(nextState){
    var checkAllProps = true
    const regex = /.*[@].*[\.].*/g //email check
    const dateRegex = /[0-9]{4}[-][0-1][0-9][-][0-3][0-9]/g //date check YYYY-MM-DD
    if(nextState.name == ''){
      checkAllProps = false
      this.state.nameError = true
    }
    else{
      this.state.nameError = false
    }
    if(nextState.fam == ''){
      checkAllProps = false
      this.state.famError = true
    }
    else{
      this.state.famError = false
    }

    var matdate = dateRegex.exec(nextState.date)
    if(matdate == null){
      checkAllProps = false
      this.state.dateError = true
    }
    else{
      var parsed = new Date(matdate[0])
      var nowDate = Date.now();

      if((isNaN(parsed)) || (parsed > nowDate)){
        checkAllProps = false
        this.state.dateError = true
      }
      else{
        this.state.dateError = false
      }
    }
    
    var mat = regex.exec(this.state.mail)
    if(mat == null){
      checkAllProps = false
      this.state.mailError = true
    }
    else{
      this.state.mailError = false
    }

    if((this.state.pass == '') || (this.state.pass.length < 8)){
      checkAllProps = false
      this.state.passError = true
    }
    else{
      this.state.passError = false
    }

    if(!this.state.confirm){
      checkAllProps = false
      this.state.confirmError = true
    }
    else{
      this.state.confirmError = false
    }

    if(checkAllProps){
      this.state.disabledButton = false
    }
    else{
      this.state.disabledButton = true
    }
  }

  handleName(event) {
    var nextState = this.state
    this.setState({name: event.target.value})
    
    nextState.name =  event.target.value
    this.checkForm(nextState)
  }
  handleFam(event) {
    var nextState = this.state
    this.setState({fam: event.target.value})
    
    nextState.fam =  event.target.value
    this.checkForm(nextState)
  }
  handleDate(event) {
    var nextState = this.state
    this.setState({date: event.target.value})
    
    nextState.date =  event.target.value
    this.checkForm(nextState)
  }
  handleMail(event) {
    var nextState = this.state
    this.setState({mail: event.target.value})
    
    nextState.mail =  event.target.value
    this.checkForm(nextState)
  }
  handlePhone(event) {
    var nextState = this.state
    this.setState({phone: event.target.value})
    
    nextState.phone =  event.target.value
    this.checkForm(nextState)
  }
  handlePass(event) {
    var nextState = this.state
    this.setState({pass: event.target.value})
    
    nextState.pass =  event.target.value
    this.checkForm(nextState)
  }
  handleConfirm(event) {
    var nextState = this.state
    this.setState({confirm: event.target.checked})
    
    nextState.confirm =  event.target.checked
    this.checkForm(nextState)
  }

  handleSubmit(event) {
    event.preventDefault()

    var users = this.state.users

    var newUser = {
      name:this.state.name,
      fam: this.state.fam,
      mail: this.state.mail
    }

    users.push(newUser)
    this.setState({users: users})

    clearState(this)

    event.preventDefault()
  }

  handleClear(event) {
    clearState(this)
  }

  render() {
    return (
      <div>
        <div>
          <h1>Список пользователей</h1>
          <table>
            <thead>
              <tr>
                <td>Имя</td>
                <td>Фамилия</td>
                <td>Email</td>
              </tr>
            </thead>
            <UsersList users={this.state.users} />
          </table>
        </div>
        <form onSubmit={this.handleSubmit} className="row col-xs-4">
          <div>
            <label>
              Имя*
            </label>
            <input className={this.state.nameError ? 'form-control error':'form-control'} type="text" value={this.state.name} onChange={this.handleName} />
          </div>
          <div className="clearfix"></div>
          <div>
            <label>
              Фамилия*
            </label>
            <input className={this.state.famError ? 'form-control error':'form-control'} type="text" value={this.state.fam} onChange={this.handleFam} />
          </div>
          <div className="clearfix"></div>
          <div>
            <label>
              Дата рождения
            </label>
            <input className={this.state.dateError ? 'form-control error':'form-control'} type="date" value={this.state.date} onChange={this.handleDate} />
            
          </div>
          <div>
            <label>
              Email*
            </label>
            <input className={this.state.mailError ? 'form-control error':'form-control'} type="email" value={this.state.mail} onChange={this.handleMail} />
            
          </div>
          <div>
            <label>
              Телефон
            </label>
            <input className={this.state.phoneError ? 'form-control error':'form-control'} type="tel" value={this.state.phone} onChange={this.handlePhone} />
            
          </div>
          <div>
            <label>
              Пароль:
            </label>
            <input className={this.state.passError ? 'form-control error':'form-control'} type="password" value={this.state.pass} onChange={this.handlePass} />
            
          </div>
          <div>
            <label>
              Подтверждаю корректность данных*
            </label>
            <input className={this.state.confirmError ? 'form-control error':'form-control'} type="checkbox" checked={this.state.confirm} onChange={this.handleConfirm} />
          </div>
          <div>
            <input type="submit" value="Отправить" className="btn btn-default form-control" disabled={this.state.disabledButton} />
          </div>
          <div>
            <input type="button" value="Очистить" className="btn btn-default form-control" onClick={() => this.handleClear()} />
          </div>
        </form>
      </div>
    );
  }
}

class App extends Component {

  render() {
    return (
      <div>
          <nav className="navbar navbar-inverse">
            <div className="container">
              <div className="navbar-header">
                <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span className="sr-only">Toggle navigation</span>
                </button>
                <a className="navbar-brand" href="/">Test project with form</a>
              </div>
              <div id="navbar" className="collapse navbar-collapse">
                <ul className="nav navbar-nav">
                  <li className="active"><a href="/">Test project</a></li>
                </ul>
              </div>
            </div>
          </nav>
          <div className="container">
            <NameForm />  
          </div>
      </div>
    );
  }
}

export default App;